package com.example.imcgarciacomfoto;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText editTextAltura;
    EditText editTextPeso;
    TextView TxtResultado;
    Button calcular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TxtResultado = findViewById(R.id.TxtResultado);
        editTextPeso = findViewById(R.id.editTextPeso);
        editTextAltura = findViewById(R.id.editTextAltura);
        calcular=findViewById(R.id.BtnCalcular);
        calcular.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick (View view) {

                double Altura = Double.parseDouble(editTextAltura.getText().toString());
                double Peso = Double.parseDouble(editTextPeso.getText().toString());
                double IMC = Peso / (Altura * Altura);

                ImageView img = findViewById(R.id.IMCView);


                if (IMC < 18.5) {

                    TxtResultado.setText("Abaixo do peso");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.abaixopeso));

                } else if (IMC < 24.9) {

                    TxtResultado.setText("Peso Ideal");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.normal));

                } else if (IMC < 29.9) {

                    TxtResultado.setText("Levemente acima do peso");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.sobrepeso));

                } else if (IMC < 34.9) {

                    TxtResultado.setText("Obesidade grau 1");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.obesidade1));


                } else if (IMC < 39.9) {

                    TxtResultado.setText("Obesidade grau 2 (Severa)");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.obesidade2));


                } else {

                    TxtResultado.setText("Obesidade grau 3 (Mórbida)");
                    img.setImageDrawable(getResources().getDrawable(R.drawable.obesidade3));


                }
            }
        });

    }
}

